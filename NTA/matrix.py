import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

plt.rc('text.latex', preamble=r'\usepackage[version=4]{mhchem}\usepackage{siunitx}')

N = 100

data = pd.read_csv('data/NTA_temp.csv', header=0)

T = np.linspace(250, 600, N)
x = np.linspace(0.35, 1, N)

#conv = np.reshape(data['NH3'] / data['NO'] * 100, (N, N))

#NH3 = np.reshape(data['NH3'], (N, N))

compounds = ['H2', 'NO', 'NH3', 'H2O', 'N2']

H2 = 2000 - np.array(data['NO'])
#x = H2 / 2000

#T = data['T']

for i in compounds:
    if(i == 'H2' or i == 'NO'):
        conv = np.reshape((1 - data[i] / data[i + 'in']) * 100, (N, N))
    else:
        conv = np.reshape(data[i] / data['NOin'] * 100, (N, N))
    
    fig, ax = plt.subplots()

    plt.xlabel(r'$x_\text{NO}$ / -', fontsize=16)
    plt.ylabel(r'$\theta$ / \si{\celsius}', fontsize=16)

    CS = plt.contourf(x, T, conv, 10, cmap='viridis')
    cb = fig.colorbar(CS, ax=ax)
    cb.set_label(r'$X_{\ce{' + i + '}}$ / \%', fontsize=16)

    plt.tight_layout()
    plt.savefig('plots/NTA_temp_' + i + '.pdf')
    plt.close('all')
