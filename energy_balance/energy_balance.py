import numpy as np

# --- Cracker ---

## - HC -
Tin  = 25 + 273.15     # [K]
Tout = 426.85 + 273.15 # [K]
P    = 1e5             # [Pa]
Q    = 883465.167539   # [kJ h-1]

## - CRACKER -
QC   = -2744828.171038 # [kJ h-1]

CRACKER = Q + QC


# --- Engine ---

## - HE -
# We don't heat up the mixture for the engine
Q = 5713729.3613563152 # [kJ h-1]

## - ENGINE -
QE = -7090169.7886027135 # [kJ h-1]

ENGINE = Q + QE


# --- NTA ---

## - HN -
Q  = -4157506.1561500761 # [kJ h-1]

## - NTA -
QN = 10838364.301637271 # [kJ h-1]

NTA = Q + QN


# --- RECYCLE ---

RECYCLE = -5767912.1084025539 # [kJ h-1]
