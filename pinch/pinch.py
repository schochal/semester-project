import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import json

linetypes = ['-', '--', '-.', ':']

with open("data.json", "rb") as f:
    data = json.load(f)
f.close()

last = 0
for i, stream in enumerate(data):
    if(stream['type'] == 'cold'):
        continue
    n = last - stream['Q1'] + stream['Q2']
    plt.plot([last / 1000, n / 1000],  [stream['T1'], stream['T2']], linewidth=1, color='blue' if stream['type'] == 'cold' else 'red', linestyle=linetypes[i], label=stream['name'])
    last = n

func = interp1d([data[0]['T1'], data[0]['T2']], [0, data[0]['Q2'] - data[0]['Q1']])
xvalue = func(data[-1]['T2']+5)
last = xvalue + data[-1]['Q1'] - data[-1]['Q2']
n = last - data[-1]['Q1'] + data[-1]['Q2']
plt.plot([last / 1000, n / 1000],  [data[-1]['T1'], data[-1]['T2']], linewidth=1, color='blue', label=data[-1]['name'])


plt.xlabel(r'Heat Load $W$ / MW', fontsize=16)
plt.ylabel(r'Temperature $\theta$ / \si{\celsius}', fontsize=16)

plt.tight_layout()
plt.legend()
plt.savefig('pinch.pdf')
plt.close('all')
