import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('../data/vary_temp.csv', header=0)

plt.plot(data['T'], data['NO'], linewidth=1, color='black', label=r'NO')
plt.plot(data['T'], data['NO2'], linewidth=1, color='black', label=r'NO\textsubscript{2}', linestyle='--')
plt.plot(data['T'], data['N2O'], linewidth=1, color='black', label=r'N\textsubscript{2}O', linestyle='-.')
plt.plot(data['T'], data['H2O'], linewidth=1, color='black', label=r'H\textsubscript{2}O', linestyle=':')

plt.xlabel(r'$T$ / $^\circ$C', fontsize=16)
plt.ylabel(r'$x_i$ / -', fontsize=16)

plt.legend()
plt.tight_layout()
plt.savefig('../plots/vary_temp.pdf')
plt.close('all')
