import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

N = 100

data = pd.read_csv('../data/matrix.csv', header=0)

T   = np.linspace(400, 1000, N)
ATF = np.linspace(12.1, 1.5125, N)

NO = np.reshape(data['NO'], (N, N))
HD = np.reshape(data['HD'], (N, N))
X  = np.reshape(data['X'], (N, N))

fig, ax = plt.subplots()

plt.xlabel(r'$T$ / $^\circ$C', fontsize=16)
plt.ylabel(r'Air to Fuel Ratio', fontsize=16)

#CS = plt.contourf(T, ATF, np.flip(NO.transpose(), axis=0), 10, cmap='viridis')
CS = plt.contourf(T, ATF, NO.transpose(), 10, cmap='viridis')
cb = fig.colorbar(CS, ax=ax)
cb.set_label(r'$x_\text{NO}$ / -', fontsize=16)

plt.tight_layout()
plt.savefig('../plots/matrix.pdf')
plt.close('all')


fig, ax = plt.subplots()

plt.xlabel(r'$T$ / $^\circ$C', fontsize=16)
plt.ylabel(r'Air to Fuel Ratio', fontsize=16)

CS = plt.contourf(T, ATF, HD.transpose(), 10, cmap='viridis')
cb = fig.colorbar(CS, ax=ax)
cb.set_label(r'Heat Duty / kW', fontsize=16)

plt.tight_layout()
plt.savefig('../plots/HD.pdf')
plt.close('all')


fig, ax = plt.subplots()

plt.xlabel(r'$T$ / $^\circ$C', fontsize=16)
plt.ylabel(r'Air to Fuel Ratio', fontsize=16)

CS = plt.contourf(T, ATF, X.transpose(), 10, cmap='viridis')
cb = fig.colorbar(CS, ax=ax)
cb.set_label(r'Conversion / -', fontsize=16)

plt.tight_layout()
plt.savefig('../plots/X.pdf')
plt.close('all')
