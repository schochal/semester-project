import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

N = 50

data = pd.read_csv('../data/flows_with_cracker.csv', header=0)

SR   = np.linspace(0.1, 1, N)
NAIR = np.linspace(2, 10, N)

HD = np.reshape(data['HD'], (N, N))
HDCR = np.reshape(data['HDCR'], (N, N))
HDHC = np.reshape(data['HDHC'], (N, N))
HDHE = np.reshape(data['HDHE'], (N, N))
XNO = np.reshape(data['XNO'], (N, N))
NH3IN = np.reshape(data['NH3IN'], (N, N))
NAIR_RES = np.reshape(data['NAIR'], (N, N))

#ATF = NAIR_RES / NH3IN
ATF = NH3IN / NAIR_RES

fig, ax = plt.subplots()

plt.xlabel(r'Split Ratio / -', fontsize=16)
plt.ylabel(r'$\dot{n}_\text{Air}$ / \si{\kilo\mol\per\hour}', fontsize=16)

CS = plt.contourf(SR, NAIR, (-HD+HDCR+HDHE+HDHC).transpose(), 10, cmap='viridis')
cb = fig.colorbar(CS, ax=ax)
cb.set_label(r'Heat Duty / kW', fontsize=16)

plt.tight_layout()
plt.savefig('../plots/FWC_HD.pdf')
plt.close('all')


fig, ax = plt.subplots()

plt.xlabel(r'Split Ratio / -', fontsize=16)
plt.ylabel(r'$\dot{n}_\text{Air}$ / \si{\kilo\mol\per\hour}', fontsize=16)

CS = plt.contourf(SR, NAIR, XNO.transpose(), 10, cmap='viridis')
cb = fig.colorbar(CS, ax=ax)
cb.set_label(r'$x_\text{NO}$', fontsize=16)

plt.tight_layout()
plt.savefig('../plots/FWC_XNO.pdf')
plt.close('all')


fig, ax = plt.subplots()

plt.xlabel(r'Split Ratio / -', fontsize=16)
plt.ylabel(r'$\dot{n}_\text{Air}$ / \si{\kilo\mol\per\hour}', fontsize=16)

CS = plt.contourf(SR, NAIR, ATF.transpose(), 10, cmap='viridis')
cb = fig.colorbar(CS, ax=ax)
cb.set_label(r'Fuel to Air Ratio', fontsize=16)

plt.tight_layout()
plt.savefig('../plots/FWC_ATF.pdf')
plt.close('all')
