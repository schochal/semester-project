\contentsline {section}{\numberline {1}Introduction}{3}{section.1}%
\contentsline {subsection}{\numberline {1.1}Importance of Ammonia as Carbon-free Fuel}{3}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Aim of this Study}{3}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}State of the Art}{3}{subsection.1.3}%
\contentsline {subsection}{\numberline {1.4}Ammonia Production Pathways}{4}{subsection.1.4}%
\contentsline {subsection}{\numberline {1.5}NOx-to-Ammonia Process (NTA)}{5}{subsection.1.5}%
\contentsline {subsection}{\numberline {1.6}Case Study}{6}{subsection.1.6}%
\contentsline {subsection}{\numberline {1.7}Scenarios}{6}{subsection.1.7}%
\contentsline {subsection}{\numberline {1.8}Rough Setup}{7}{subsection.1.8}%
\contentsline {section}{\numberline {2}Procedure}{7}{section.2}%
\contentsline {subsection}{\numberline {2.1}Cracker}{7}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Engine}{8}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Kinetic Model for Combustion}{9}{subsubsection.2.2.1}%
\contentsline {subsection}{\numberline {2.3}NTA}{9}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.4}Recycle}{10}{subsection.2.4}%
\contentsline {subsection}{\numberline {2.5}Energy Integration}{11}{subsection.2.5}%
\contentsline {subsection}{\numberline {2.6}Economic Assessement}{11}{subsection.2.6}%
\contentsline {subsubsection}{\numberline {2.6.1}CAPEX}{11}{subsubsection.2.6.1}%
\contentsline {subsubsection}{\numberline {2.6.2}OPEX}{11}{subsubsection.2.6.2}%
\contentsline {subsection}{\numberline {2.7}Life Cycle Assessement}{11}{subsection.2.7}%
\contentsline {section}{\numberline {3}Results \& Discussion}{12}{section.3}%
\contentsline {subsection}{\numberline {3.1}Heat Exchanger Network}{13}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Economic Assessement}{13}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Life Cycle Assessment}{16}{subsection.3.3}%
\contentsline {section}{\numberline {4}Conclusion}{17}{section.4}%
\contentsline {subsection}{\numberline {4.1}Dangers of Ammonia as a Maritine Fuel}{18}{subsection.4.1}%
\contentsline {section}{\numberline {A}Appendix}{22}{appendix.A}%
\contentsline {subsection}{\numberline {A.1}Code}{22}{subsection.A.1}%
