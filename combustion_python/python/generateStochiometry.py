#
# Author: Alexander Schoch <schochal@student.ethz.ch>
# Date:   April 27, 2023
#
# This python script reads the raw data from
# http://dx.doi.org/10.1016/j.combustflame.2012.06.003 (table from suppl.
# material in ../data/raw_data.json) and converts it to a stochiometric
# coefficients table. the prefix r_ means reactand and p_ means product.
#

import json

RAW_DATA_FILE = '../data/raw_data.json'
COMPOUND_MAPPING_FILE = '../data/compound_mapping.json'
OUTPUT_FILE = '../data/stochiometry.csv'

with open(RAW_DATA_FILE, "rb") as f:
    json_data = json.load(f)
f.close()

with open(COMPOUND_MAPPING_FILE, "rb") as f:
    mapping = json.load(f)
f.close()

# get all possible compounds
compounds = [mapping[i]['compound'] for i in range(len(mapping))]

# print header to new CSV file
output = open(OUTPUT_FILE, "w")

## For each compound, prepend "r_" and "p_" for reactant and product, respecively
output.write('nr,reaction,')
output.write(','.join(['r_' + c for c in compounds]))
output.write(',')
output.write(','.join(['p_' + c for c in compounds]))
output.write('\n')

for i in range(len(json_data)):
    nr       = json_data[i]['nr']
    reaction = json_data[i]['reaction']
    A        = json_data[i]['A']
    n        = json_data[i]['n']
    Ea       = json_data[i]['Ea']

    output.write(str(nr) + ',' + reaction)

    reaction_parts = reaction.split('=')
    reactants      = reaction_parts[0].split('+')
    products       = reaction_parts[1].split('+')

    # for each compound, check how many reactants are this compound. write this value to file
    for c in compounds:
        ctr = 0
        for r in reactants:
            if r == c:
                ctr+=1
        output.write(',' + str(ctr))

    # for each compound, check how many products are this compound. write this value to file
    for c in compounds:
        ctr = 0
        for p in products:
            if p == c:
                ctr+=1
        output.write(',' + str(ctr))
    output.write('\n')

output.close()
