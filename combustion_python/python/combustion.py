#
# Author: Alexander Schoch <schochal@student.ethz.ch>
# Date:   April 27, 2023
#
# This script reads the file ../data/stochiometry.csv and integrates all
# reactions for N steps using a fourth order runge-kutta integration scheme
#

import json
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from progress.bar import ShadyBar

plt.rc('text.latex', preamble=r'\usepackage{sfmath}\usepackage{siunitx}\DeclareSIUnit\USD{USD}\DeclareSIUnit\ton{t}\DeclareSIUnit\week{w}\DeclareSIUnit\year{y}\usepackage[version=4]{mhchem}')
plt.rc('font', family='sans-serif')

# specify data files here
RAW_DATA_FILE = '../data/raw_data.json'
COMPOUND_MAPPING_FILE = '../data/compound_mapping.json'
STOCHIOMETRY_FILE = '../data/stochiometry.csv'
INITIAL_CONDITION_FILE = '../data/IC.csv'

RESULTS_FILE = '../data/results.csv'

prev_results = pd.read_csv(RESULTS_FILE, header=0)

R = 8.314 # [J / mol K]

#N = 200
N = 1
dt = 1e-16
T = 651 + 273.15 # [K]

bar = ShadyBar('Progress', max=N, suffix='%(percent).1f%% - %(eta)ds remaining')

tarr = np.arange(0,N*dt+dt/2,dt)

with open(RAW_DATA_FILE, "rb") as f:
    json_data = json.load(f)
f.close()

with open(COMPOUND_MAPPING_FILE, "rb") as f:
    mapping = json.load(f)
f.close()

stoich = pd.read_csv(STOCHIOMETRY_FILE, header=0)
IC = pd.read_csv(INITIAL_CONDITION_FILE, header=None)

c = np.zeros((N+1, len(IC)))
c[0] = prev_results.transpose()[len(prev_results)-1]

# get all possible compounds
compounds = [mapping[i]['compound'] for i in range(len(mapping))]
indices   = {}
for i in range(len(compounds)):
    indices[compounds[i]] = i

def k(T, A, n, Ea):
    return A * T**n * np.exp(-Ea / (R * T))

def print_table(c, compounds = compounds, indices = indices, selection = ['NH3', 'O2', 'H2', 'H2O'], pit = [-1]):
    sel_ind = [indices[s] for s in selection]           # convert selection to indices
    pit_ind = [0, *[range(N)[ti] + 1 for ti in pit]]    # convert point in time (pit) to indices, also include IC
    c_red = c.transpose()[sel_ind].transpose()[pit_ind] # use selection and pit to generate a reduced matrix
    df = pd.DataFrame(c_red, columns = selection, index = ['IC', *[str(i) for i in pit]]) # convert to DataFrame

    df = df.replace(0.0, "")
    print(df)

def append_to_file(c, filename):
    last_line = c[-1]
    last_line_df = pd.DataFrame(last_line, index = compounds, columns = [len(prev_results)]).transpose()
    df = pd.concat([prev_results, last_line_df])

    df.to_csv(filename, index = False)

def generate_plot(c, filename, compounds = compounds, indices = indices, selection = ['NH3', 'O2', 'H2', 'H2O'], end_time = -1):
    tarr = np.arange(0, prev_results.shape[0]*dt - dt / 2, dt)
    linestyles = ['-', '--', '-.', ':', '-', '--', '-.', ':']
    colors = ['black', 'black', 'black', 'black', 'red', 'red', 'red', 'red']
    for i, s in enumerate(selection):
        plt.plot(tarr[:end_time], c.transpose()[s][:end_time], linewidth=1, label=r'\ce{' + s + r'}', linestyle=linestyles[i], color=colors[i])

    plt.xlabel(r'$t$ / s', fontsize=16)
    plt.ylabel(r'$x$ / -', fontsize=16)

    plt.legend(loc='right')
    plt.tight_layout()
    plt.savefig(filename)
    plt.close('all')

for i in range(1,N+1): # for each time step
    bar.next()

    cv = c[i-1] # current values
    dc = np.zeros((len(IC)))

    for j in range(len(stoich)): # for each reaction
        row = stoich.transpose()[j]
        # rate constant using arrhenius
        rc = k(
            T,
            json_data[row['nr']-1]['A'],
            json_data[row['nr']-1]['n'],
            json_data[row['nr']-1]['Ea'] / 4.184
        ) # 4.184 is the conversion factor cal -> J/mole

        # for each compound
        for comp in range(len(compounds)):
            nu_total = -row['r_' + compounds[comp]] + row['p_' + compounds[comp]] #moles
            const = 1
            for comp2 in range(len(compounds)):
                 # All compounds except the one we are looking at are constants
                 # in the ODE
                if(compounds[comp2] != compounds[comp]):
                    const *= cv[comp2]**row['r_' + compounds[comp2]] #moles
            power = row['r_' + compounds[comp]]
            lamb = nu_total * const * rc
            conc = cv[comp]
            # RK4: Fourth other Runge Kutta scheme for numeric integration of ODE - explicit RK4 (maybe use scipy.integrate.odeint for implicit RK4)
            k1 = lamb * conc**power
            k2 = lamb * (conc + dt*k1/2)**power
            k3 = lamb * (conc + dt*k2/2)**power
            k4 = lamb * (conc + dt*k3)**power

            dcterm = dt / 6 * (k1 + 2*k2 + 2*k3 + k4)
            dc[comp] += dcterm

    c[i] = [cv[j] + dc[j] for j in range(len(compounds))] #new total moles for this iteration
bar.finish()
append_to_file(c, RESULTS_FILE)


#unstable values for dt larger than 1E-16 -- 1E-15 gives nan values
