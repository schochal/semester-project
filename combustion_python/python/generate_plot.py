import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import datetime

now = datetime.datetime.now()
now_str = now.strftime("%Y%m%d_%H%M%S")

plt.rc('text.latex', preamble=r'\usepackage{siunitx}\DeclareSIUnit\USD{USD}\DeclareSIUnit\ton{t}\DeclareSIUnit\week{w}\DeclareSIUnit\year{y}')
plt.rc('font', family='sans-serif')

RESULTS_FILE = '../data/results.csv'
data = pd.read_csv(RESULTS_FILE, header=0)

selection = ['NH3', 'H2', 'O2', 'NH', 'NO', 'NO2', 'N2O', 'H2O']

tarr = np.arange(0, len(data))
end_time = len(tarr)

for s in selection:
    plt.plot(tarr[:end_time], data[s][:end_time], linewidth=1, label=s)

plt.xlabel(r'$t$ / arbitrary units', fontsize=16)
plt.ylabel(r'$x$ / -', fontsize=16)

plt.legend()
plt.tight_layout()
plt.savefig('../plots/' + now_str + '.pdf')
plt.close('all')
