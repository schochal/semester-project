import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import pandas as pd

plt.rc('text.latex', preamble=r'\usepackage{sfmath}\usepackage{siunitx}\DeclareSIUnit\USD{USD}\DeclareSIUnit\ton{t}\DeclareSIUnit\week{w}\DeclareSIUnit\year{y}')
plt.rc('font', family='sans-serif')

# === CAPEX ===
class Block:
    def __init__(self, S, S_unit, S_lower, S_upper, a, b, n):
        self.S        = S
        self.S_unit   = S_unit
        self.S_lower  = S_lower
        self.S_upper  = S_upper
        self.a        = a
        self.b        = b
        self.n        = n

    def __call__(self):
        print(self.calc())

    def __str__(self):
        return str(self.calc()) + ' USD'

    def calc(self):
        if(self.S < self.S_lower or self.S > self.S_upper):
            print(self.type + ':', 'S should be between', self.S_lower, self.S_unit, 'and', self.S_upper, self.S_unit)
        return self.a + self.b * self.S**self.n

class HE(Block):
    # U-tube shell and tube, Sinnot/Towler pp. 304
    def __init__(self, area):
        self.S = area
        self.S_unit = 'm2'
        self.S_lower = 10
        self.S_upper = 1e3
        self.a       = 24e3
        self.b       = 46
        self.n       = 1.2
        self.type    = 'HE'

class Reactor(Block):
    # Jacketed, agitated, Sinnot/Towler pp. 304
    def __init__(self, V):
        self.S = V
        self.S_unit  = 'm3'
        self.S_lower = 0.5
        self.S_upper = 100
        self.a       = 53e3
        self.b       = 28e3
        self.n       = .8
        self.type    = 'Reactor'

class Tank(Block):
    # cone roof, Sinnot/Towler pp.304
    def __init__(self, V):
        self.S = V
        self.S_unit  = 'm3'
        self.S_lower = 10
        self.S_upper = 4e3
        self.a       = 5e3
        self.b       = 1.4e3
        self.n       = .7
        self.type    = 'Tank'

class Flash(Block):
    # vertical pressure vessel, cs, Sinnot/Towler pp.304
    def __init__(self, V):
        self.S = V
        self.S_unit  = 'kg'
        self.S_lower = 160
        self.S_upper = 250000
        self.a       = 10000
        self.b       = 29
        self.n       = .85
        self.type    = 'Flash'

s = 0

# --- TANKS ---
n_CRACKER = 3848.59539655812 # [kmol h-1]
n_ENGINE  = 1882.339348 # [kmol h-1]
n_RECYCLE = 1434.1270440482 # [kmol h-1]

n_tot = n_CRACKER + n_ENGINE - n_RECYCLE
base_case = 2 * n_ENGINE
diff = n_tot - base_case
dt = 1 # [weeks]
dt_h = dt * 7 * 24
n_NH3 = dt_h * diff # [kmol]

tanks = 0
M_NH3 = 17 # [g mol-1]
D_NH3 = 600 # [kg m-3] [Yapicioglu/Dincer, p. 105]
D_Diesel = 850 # [kg m-3] [Yapicioglu/Dincer p. 105]
V_NH3 = n_NH3 * M_NH3 / D_NH3 # [m3]
tanks += Tank(V_NH3).calc()
s += tanks

# --- Heat Exchangers ---
heat_exchangers = 0
heat_exchangers += HE(264.8).calc() + 5 * HE(1000).calc()  # SW - NTA
heat_exchangers += HE(754).calc() + 19 * HE(1000).calc()   # CR - NTA
heat_exchangers += HE(282.1).calc() + 14 * HE(1000).calc() # SW - FL1
heat_exchangers += HE(339.8).calc() + HE(1000).calc()      # SW - FL1
heat_exchangers += HE(588.7).calc() + 5 * HE(1000).calc()  # SW - FL2
heat_exchangers += HE(443.3).calc() + 5 * HE(1000).calc()  # NTA - CR
heat_exchangers += HE(532.5).calc()                        # SW - FL1
s += heat_exchangers

# --- CRACKER ---
reactors = 0
reactors += Reactor(22.27 * 0.95**2/4 * np.pi).calc() # Volume of cylinder

# --- NTA ---
V_flow = 267.139843546193 # [m3 s-1]
tau = 6 # [s]
reactors += Reactor(100).calc()*16 # PFR
s += reactors

# --- RECYCLE ---
flashes = 0
flashes += Flash(48635.2808088285).calc() # Flash 1
flashes += Flash(40578.48431453).calc() # Flash 2
s += flashes

CEPCI_2007 = 525.4
CEPCI_2023 = 803.4

s *= CEPCI_2023 / CEPCI_2007
correction_factors = [
    0.8, # Piping (FPP)
    0.3, # Instrumentation and Control (FIC)
    0.2, # Electrical Infracstucture (FE)
    0,   # Civil Engineering (FCE)
    0,   # Buildings (FB)
    0.1, # Painting and Coating (FPC)
    0.3, # Equipment Edification (FEE)
]
s *= 1 + sum(correction_factors)
CAPEX = s
print('CAPEX =', round(CAPEX / 1000), 'kUSD')

labels = ['Heat Exchangers', 'Tanks', 'Reactors', 'Flashes']
fractions = np.array([heat_exchangers, tanks, reactors, flashes])
fractions /= sum(fractions) # normalize
fractions *= 100
hatches = ['////////', '....', r'\\\\\\\\', 'oo']
colors = ['white' for i in range(len(labels))]

fig, ax = plt.subplots(figsize=(4.8, 4.8))
wedges, _, _ = ax.pie(fractions, labels=labels, colors=colors, autopct=lambda pct: r"\SI{{{:.1f}}}{{\percent}}".format(round(pct, 1)), textprops={'backgroundcolor': (1, 1, 1, .9), 'alpha': 1})
for i in range(len(labels)):
    wedges[i].set(hatch=hatches[i], fill=False)
fig.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
ax.axis('equal')
ax.margins(0, 0)
plt.tight_layout()
plt.savefig('plots/CAPEX.pdf')
plt.close('all')


# === OPEX ===

ammonia_prices = {
    "gray ammonia": {
        " FOB Middle East": 246.59,
        " FOB Black Sea": 263.64,
        " CFR Far East Asia": 340.00,
        " CFR USGC": 327.73,
        " CFR Northwest Europe": 351.59
    },
    "blue ammonia": {
        "FOB Middle East": 276.64,
        "CFR Far East Asia": 370.04,
        "CFR USGC": 351.84,
        "CFR Northwest Europe": 388.51
    },
    "green ammonia": {
        "Dlvd Northwest Europe from EC Canada": 749.06,
        "Dlvd Far East Asia from WC Canada": 767.81,
        "Dlvd Far East Asia from Australia": 774.92,
        "Dlvd Northwest Europe from US Gulf Coast": 781.58,
        "Dlvd Far East Asia from Middle East": 781.65,
        "Dlvd Northwest Europe from Middle East": 796.65
    }
}

average_ammonia_prices = {}
for t in ammonia_prices:
    s = 0
    c = 0
    for i in ammonia_prices[t]:
        if "FOB" in i: # exclude free on board prices, as they are without shipping
            continue
        s += ammonia_prices[t][i]
        c += 1
    average_ammonia_prices[t] = s / c

colors = []
heights = []
labels = []

for t in ammonia_prices:
    color = t.split(' ')[0]
    for e in ammonia_prices[t]:
        colors.append(color)
        heights.append(ammonia_prices[t][e])
        labels.append(e)
sort = []
for i in range(len(colors)):
    sort.append({"label": labels[i], "height": heights[i], "color": colors[i]})
sort.sort(key = lambda x: x['height'])

plt.barh([i['label'] for i in sort], [i['height'] for i in sort], color='white', alpha=.8, hatch='////////', edgecolor=[i['color'] for i in sort])

gray = mpatches.Patch(color='white', ec='gray', label='Gray Ammonia', hatch='////////', alpha=.8)
blue = mpatches.Patch(color='white', ec='blue', label='Blue Ammonia', hatch='////////', alpha=.8)
green = mpatches.Patch(color='white', ec='green', label='Green Ammonia', hatch='////////', alpha=.8)
plt.legend(handles=[gray, blue, green])

plt.xlabel(r'Ammonia Price / USD\,t$^{-1}$', fontsize=16)
plt.tight_layout()
plt.savefig('plots/ammonia_prices.pdf')
plt.close('all')

## FOB: Free on Board: Shipping price NOT included
## Cost and Freight: Shipping price included
## USGC: US gulf coast
## https://www.spglobal.com/commodityinsights/en/market-insights/latest-news/energy-transition/051023-interactive-ammonia-price-chart-natural-gas-feedstock-europe-usgc-black-sea

diesel_prices = pd.read_csv('data/diesel.csv', header=None)
labels_to_show = ['Japan', 'USA', 'Germany', 'Russia', 'Norway', 'India', 'Singapore', 'Hong Kong']
label_indices = [np.where(diesel_prices[0] == i)[0][0] for i in labels_to_show]

average = np.mean(diesel_prices[1]) * 1000

plt.bar(diesel_prices[0], diesel_prices[1] * 1000, color='black', alpha=1, width=1, ec='black')
plt.plot([0,len(diesel_prices[0])], [average, average], linewidth=1, linestyle='-', color='white')
plt.plot([0,len(diesel_prices[0])], [average, average], linewidth=1, linestyle=':', color='black')
plt.text(0, average, r'Global Average: \SI{' + str(round(average)) + r'}{USD}', horizontalalignment='left', verticalalignment='bottom')
plt.xticks(label_indices, rotation=45, ha="right")
plt.xlim(-0.5, len(diesel_prices[0])-.5)

plt.ylabel(r'Diesel Price / USD\,t$^{-1}$', fontsize=16)
plt.tight_layout()
plt.savefig('plots/diesel_prices.pdf')
plt.close('all')

# diesel price (24-Jul-2023): https://www.globalpetrolprices.com/diesel_prices/

diesel_usage = 1971.9 # [t week-1]
#diesel_usage = 1763.5 # [t week-1] [if efficienies are used]
diesel_cost = diesel_usage * average # [USD week-1]
diesel_cost_per_year = diesel_cost * 0.777 * 52 # [USD y-1]
#ammonia_usage = (1.5059e+04 + 65542.8458482232 - 24423.9866712855) / 1000 * 24 * 7 # [t week-1]
ammonia_usage = (31999.768916 + 65542.8458482232 - 24423.9866712855) / 1000 * 24 * 7 # [t week-1]
ammonia_usage_bc = 2 * 31999.768916 / 1000 * 24 * 7 # [t week-1]
ammonia_cost = {i: average_ammonia_prices[i] * ammonia_usage for i in average_ammonia_prices} # [USD week-1]
ammonia_cost_bc = {i: average_ammonia_prices[i] * ammonia_usage_bc for i in average_ammonia_prices} # [USD week-1]
ammonia_cost_per_year = {i: ammonia_cost[i] * 0.777 * 52 for i in ammonia_cost} # [USD y-1]
ammonia_cost_per_year_bc = {i: ammonia_cost_bc[i] * 0.777 * 52 for i in ammonia_cost_bc} # [USD y-1]
BSFC = ammonia_usage / (24 * 7 * 68640) * 1e6

DWT  = 115700 # [t]
v    = 49.078 # [km h-1]
d    = v * 24 * 7
pptk = diesel_cost / (DWT * v * 24 * 7) # [USD km-1 t-1]

diesel_usage_per_tkm = diesel_usage / (DWT * v * 24 * 7)  # [t / tkm]
ammonia_usage_per_tkm = ammonia_usage / (DWT * v * 24 * 7) # [t / tkm]
ammonia_usage_per_tkm_bc = ammonia_usage_bc / (DWT * v * 24 * 7) # [t / tkm]

plt.bar(['Diesel', *[i.title() for i in ammonia_cost.keys()]], [diesel_cost / (DWT * v * 24 * 7), *[i / (DWT * v * 24 * 7) for i in ammonia_cost.values()]], color='white', alpha=.8, hatch='////////', edgecolor=['brown', *[i.split(' ')[0] for i in ammonia_prices]])

plt.ylabel(r'Price / \si{\USD\per\ton\per\kilo\meter}', fontsize=16)
plt.tight_layout()
plt.savefig('plots/OPEX.pdf')
plt.close('all')

# === CAPEX vs. OPEX ===
ap = 15 # ammortization period [y]
interest = 0.1
ACCR = (interest * (1 + interest)**ap) / ((1 + interest)**ap - 1)
capex_per_year = CAPEX * ACCR
total_cost_per_year_diesel = diesel_cost_per_year
total_cost_per_year_ammonia = {i: ammonia_cost_per_year[i] + capex_per_year for i in ammonia_cost_per_year} # [USD y-1]
total_cost_per_year_ammonia_bc = {i: ammonia_cost_per_year_bc[i] + capex_per_year for i in ammonia_cost_per_year_bc} # [USD y-1]

labels = ['BAU', *[i.title() for i in ammonia_cost.keys()]]
bar_labels = ['NTA', 'BC']
bar_width = .4
x = list(np.arange(len(labels)))
x[0] = bar_width
x = np.array(x)

opex_convert_to_tkm = 0.777 * 52 * DWT * v * 24 * 7
capex_convert_to_tkm = 52 * DWT * v * 24 * 7

fig, ax = plt.subplots()
ax.bar(x, [diesel_cost_per_year / opex_convert_to_tkm, *[i / opex_convert_to_tkm for i in ammonia_cost_per_year.values()]], color='white', alpha=.8, hatch='////////', edgecolor=['brown', *[i.split(' ')[0] for i in ammonia_prices]], width=bar_width)
ax.bar(x, [0, capex_per_year / capex_convert_to_tkm, capex_per_year / capex_convert_to_tkm, capex_per_year / capex_convert_to_tkm], color='white', alpha=.8, hatch='....', edgecolor=['brown', *[i.split(' ')[0] for i in ammonia_prices]], bottom=[diesel_cost_per_year / opex_convert_to_tkm, *[i / opex_convert_to_tkm for i in ammonia_cost_per_year.values()]], width=bar_width)
for i in range(3):
    plt.text(x[i+1], list(ammonia_cost_per_year.values())[i] / opex_convert_to_tkm + capex_per_year / capex_convert_to_tkm, bar_labels[0], horizontalalignment='center', verticalalignment='bottom')

ax.bar(x + bar_width, [0, *[i / opex_convert_to_tkm for i in ammonia_cost_per_year_bc.values()]], color='white', alpha=.8, hatch='////////', edgecolor=['brown', *[i.split(' ')[0] for i in ammonia_prices]], width=bar_width)
ax.bar(x + bar_width, [0, capex_per_year / capex_convert_to_tkm, capex_per_year / capex_convert_to_tkm, capex_per_year / capex_convert_to_tkm], color='white', alpha=.8, hatch='....', edgecolor=['brown', *[i.split(' ')[0] for i in ammonia_prices]], bottom=[0, *[i / opex_convert_to_tkm for i in ammonia_cost_per_year_bc.values()]], width=bar_width)
for i in range(3):
    plt.text(x[i+1] + bar_width, list(ammonia_cost_per_year_bc.values())[i] / opex_convert_to_tkm + capex_per_year / capex_convert_to_tkm, bar_labels[1], horizontalalignment='center', verticalalignment='bottom')
ax.set_xticks(x + np.array([bar_width / 2 if i != 'BAU' else 0 for i in labels]), labels)

lines = mpatches.Patch(color='white', ec='black', label=r'OPEX', hatch='////////', alpha=.8)
dots  = mpatches.Patch(color='white', ec='black', label=r'CAPEX', hatch='....', alpha=.8)
plt.legend(handles=[dots, lines])

plt.ylabel(r'Total Cost / \si{\USD\per\ton\per\kilo\meter}', fontsize=16)
plt.tight_layout()
plt.savefig('plots/CAPEX_OPEX.pdf')
plt.close('all')

# === LCA ===
# hatches = {
#     'diesel production': '////////',
#     'freight ship': '....',
#     'port': r'\\\\\\\\',
#     'combustion': 'OO',
#     'natural gas': 'oooo',
#     'electricity': '|||||',
#     'process emissions': 'OO..',
#     'infrastructure': 'xxxx',
#     'CO2 capture': '++++',
#     'H2': '-----',
#     'NO from Combustion': 'xxx+++'
# }
hatches = {
    'fuel production': '////////',
    'freight ship': '....',
    'port': r'\\\\\\\\',
    'combustion emissions': 'oo',
}

# GWI = {
#     'diesel': {
#         'diesel production': 0.3600,
#         'freight ship': 0.1200,
#         'port': 0.7067,
#         'combustion': 2.3867
#     },
#     'gray ammonia': {
#         'natural gas': 0.243750,
#         'electricity': 0.418750,
#         'process emissions': 1.481250,
#         'infrastructure': 0.056250,
#         'freight ship': 0.1200,
#         'port': 0.7067,
#         'NO from Combustion': 0
#     },
#     'blue ammonia': {
#         'natural gas': 0.2375   ,
#         'electricity': 0.13125,
#         'CO2 capture': 0.04375,
#         'process emissions': 0.075,
#         'infrastructure': 0.06875,
#         'freight ship': 0.1200,
#         'port': 0.7067,
#         'NO from Combustion': 0
#     },
#     'green ammonia': {
#         'H2': 0.125,
#         'infrastructure': 0.05625,
#         'freight ship': 0.1200,
#         'port': 0.7067,
#         'NO from Combustion': 0
#     }
# } # using BAU, SMR+CCS_SG+FG, PEM-NUCLEAR
# DAngelo2021, Negri2022

GWI = {
    'bau': {
        'fuel production': 0.3600,
        'freight ship': 0.1200,
        'port': 0.7067,
        'combustion emissions': 2.3867
    },
    'gray ammonia': {
        'fuel production': 2.1999999,
        'freight ship': 0.1200,
        'port': 0.7067,
        'combustion emissions': 0
    },
    'blue ammonia': {
        'fuel production': 0.55625,
        'freight ship': 0.1200,
        'port': 0.7067,
        'combustion emissions': 0
    },
    'green ammonia': {
        'fuel production': 0.18125,
        'freight ship': 0.1200,
        'port': 0.7067,
        'combustion emissions': 0
    }
}

GWP = {i: {j: (GWI[i][j] * 1e11 / 36e12 if i == 'bau' else GWI[i][j] * ammonia_usage_per_tkm * 1000) for j in GWI[i]} for i in GWI}
GWP_bc = {i: {j: (0 if i == 'bau' else GWI[i][j] * ammonia_usage_per_tkm_bc * 1000) for j in GWI[i]} for i in GWI}

GWP_total = {i: sum([GWP[i][j] for j in GWP[i]]) for i in GWP}
labels = [i.title() if i != 'bau' else i.upper() for i in GWP ]
colors = ['brown', 'gray', 'blue', 'green']


m_NO = n_ENGINE * .1 * 30 / 1000 * 24 * 7 # [t week-1]
m_NO_bc = n_ENGINE * .05 * 30 / 1000 * 24 * 7
NO_per_tkm = m_NO / (DWT * v * 24 * 7) * 1000 # [kg / tkm]
NO_per_tkm_bc = m_NO_bc / (DWT * v * 24 * 7) * 1000 # [kg / tkm]
GWP_NOx = 8.5
CO2_eq_NOx = NO_per_tkm * GWP_NOx # [kg tkm-1]
CO2_eq_NOx_bc = NO_per_tkm_bc * GWP_NOx # [kg tkm-1]

for i in ['gray ammonia', 'blue ammonia', 'green ammonia']:
    GWP[i]['combustion emissions'] = CO2_eq_NOx
    GWP[i]['freight ship'] = GWP['bau']['freight ship']
    GWP[i]['port'] = GWP['bau']['port']
    GWP_bc[i]['combustion emissions'] = CO2_eq_NOx_bc
    GWP_bc[i]['freight ship'] = GWP['bau']['freight ship']
    GWP_bc[i]['port'] = GWP['bau']['port']

bar_width = .4
s = np.array([0, 0, 0, 0])
x = list(np.arange(len(hatches)))
x[0] = bar_width
x = np.array(x)

fig, ax = plt.subplots()

for i in hatches:
    data = np.array([GWP[j][i] if i in GWP[j] else 0 for j in GWP])
    ax.bar(x, data, color='white', ec=colors, hatch=hatches[i], bottom=s, alpha=.8, width=bar_width)
    s = s + data
for i in range(3):
    plt.text(x[i+1], s[i+1], bar_labels[0], horizontalalignment='center', verticalalignment='bottom')


s = np.array([0, 0, 0, 0])
for i in hatches:
    data = np.array([GWP_bc[j][i] if i in GWP_bc[j] else 0 for j in GWP_bc])
    ax.bar(x + bar_width, data, color='white', ec=colors, hatch=hatches[i], bottom=s, alpha=.8, width=bar_width)
    s = s + data
for i in range(3):
    plt.text(x[i+1] + bar_width, s[i+1], bar_labels[1], horizontalalignment='center', verticalalignment='bottom')

ax.set_xticks(x + np.array([bar_width / 2 if i != 'BAU' else 0 for i in labels]), labels)

patches = [mpatches.Patch(color='white', ec='black', label=(i.title() if i.islower() else i), hatch=hatches[i], alpha=.8) for i in hatches]
plt.legend(handles=patches)

#plt.ylim(0,3.6e-5)
plt.ylabel(r'Climate Change / \si{\kilo\gram\of{CO\textsubscript{2}-eq}\per\ton\per\kilo\meter}', fontsize=16)
plt.tight_layout()
plt.savefig('plots/LCA.pdf')
plt.close('all')


## Production

## Diesel combustion
# GWP_CO2 = 1    #
# GWP_NOx = 31.5
# w_CO2 = 7.1    # weight-%
# w_NOx = 0.034  # weight-%
# w_CO  = 0.043  # weight-%
# s_CO2 = w_CO2 / (w_CO2 + w_CO) # selectivity

# GWP_Diesel_CO2 = diesel_usage * s_CO2 * GWP_CO2
# GWP_Diesel_NOx = diesel_usage * s_CO2 / w_CO2 * w_NOx * GWP_NOx
# GWP_Diesel = GWP_Diesel_CO2 + GWP_Diesel_NOx

## Ammonia combustion
# CO2_emissions_gray_ammonia = 1.6 # [t t-1]
# m_NO = 5636.87366039651 / 1000 * 24 * 7 # [t week-1]

# CO2_emissions = {
#     "gray ammonia": CO2_emissions_gray_ammonia,
#     "blue ammonia": CO2_emissions_gray_ammonia * 0.15,
#     "green ammonia": 0,
# }
# GWP_Ammonia_Production = {i: CO2_emissions[i] * ammonia_usage * GWP_CO2 for i in CO2_emissions}
# GWP_Ammonia_Engine     = m_NO * GWP_NOx
# GWP_Ammonia            = {i: GWP_Ammonia_Production[i] + GWP_Ammonia_Engine for i in CO2_emissions}

# plt.bar(['Diesel', *[i.title() for i in ammonia_cost.keys()]], [GWP_Diesel_CO2, *[i for i in GWP_Ammonia_Production.values()]], color='white', alpha=.8, hatch='////////', edgecolor=['brown', *[i.split(' ')[0] for i in ammonia_prices]])
# plt.bar(['Diesel', *[i.title() for i in ammonia_cost.keys()]], [GWP_Diesel_NOx, GWP_Ammonia_Engine, GWP_Ammonia_Engine, GWP_Ammonia_Engine], color='white', alpha=.8, hatch=r'....', edgecolor=['brown', *[i.split(' ')[0] for i in ammonia_prices]], bottom=[GWP_Diesel_CO2, *[i for i in GWP_Ammonia_Production.values()]])

# lines = mpatches.Patch(color='white', ec='black', label=r'CO\textsubscript{2} contribution', hatch='////////', alpha=.8)
# dots  = mpatches.Patch(color='white', ec='black', label=r'NO$_x$ contribution', hatch='....', alpha=.8)
# plt.legend(handles=[dots, lines])

# plt.ylabel(r'Carbon Dioxide Equivalent / \si{\ton\per\week}', fontsize=16)
# plt.tight_layout()
# plt.savefig('plots/LCA.pdf')
# plt.close('all')

# plt.bar(['Diesel', *[i.title() for i in ammonia_cost.keys()]], [GWP_Diesel_CO2 + GWP_Diesel_NOx, *[i + GWP_Ammonia_Engine for i in GWP_Ammonia_Production.values()]], color='white', alpha=.8, hatch='////////', edgecolor=['brown', *[i.split(' ')[0] for i in ammonia_prices]])
# plt.yscale('log')
# plt.ylim(1e0, 1e5)
# plt.tick_params(
#     axis='y',          # changes apply to the x-axis
#     which='both',      # both major and minor ticks are affected
#     left=False,
#     labelleft=False)
# plt.ylabel(r'Carbon Dioxide Equivalent', fontsize=16)
# plt.tight_layout()
# plt.savefig('plots/LCA_log_fake.pdf')
# plt.close('all')

# plt.bar(['Diesel', *[i.title() for i in ammonia_cost.keys()]], [GWP_Diesel_CO2 + GWP_Diesel_NOx, *[i + GWP_Ammonia_Engine for i in GWP_Ammonia_Production.values()]], color='white', alpha=.8, hatch='////////', edgecolor=['brown', *[i.split(' ')[0] for i in ammonia_prices]])
# plt.yscale('log')
# plt.ylim(1e0, 1e5)
# plt.ylabel(r'Carbon Dioxide Equivalent / \si{\ton\per\week}', fontsize=16)
# plt.tight_layout()
# plt.savefig('plots/LCA_log.pdf')
# plt.close('all')
