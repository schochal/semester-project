import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

plt.rc('text.latex', preamble=r'\usepackage{sfmath}\usepackage{siunitx}\DeclareSIUnit\USD{USD}\DeclareSIUnit\ton{t}\DeclareSIUnit\week{w}\DeclareSIUnit\year{y}')
plt.rc('font', family='sans-serif')

N = 100

data = pd.read_csv('separation_water_ammonia.csv', header=0)

T1   = np.linspace(10, 70, N)
T2   = np.linspace(10, 70, N)

XNH3B1 = np.reshape(data['NNH3B1'] / (data['NNH3B1'] + data['NH2OB1']), (N, N))
XNH3T1 = np.reshape(data['NNH3T1'] / (data['NNH3T1'] + data['NH2OT1']), (N, N))

XNH3B2 = np.reshape(data['NNH3B2'] / (data['NNH3B2'] + data['NH2OB2']), (N, N))
XNH3T2 = np.reshape(data['NNH3T2'] / (data['NNH3T2'] + data['NH2OT2']), (N, N))

opt = np.reshape(data['NNH3T2']**2 / (data['NNH3T2'] + data['NH2OT2']), (N, N))


def plot(matrix, filename, label):
    fig, ax = plt.subplots()

    plt.xlabel(r'$T_1$ / \si{\celsius}', fontsize=16)
    plt.ylabel(r'$T_2$ / \si{\celsius}', fontsize=16)

    CS = plt.contourf(T1, T2, matrix.transpose(), 10, cmap='viridis')
    cb = fig.colorbar(CS, ax=ax)
    cb.set_label(label, fontsize=16)

    plt.tight_layout()
    plt.savefig('plots/' + filename)
    plt.close('all')

def plot_2d(lines, filename, labels, ylabel):
    plt.xlabel(r'$T$ / \si{\celsius}', fontsize=16)
    plt.ylabel(ylabel, fontsize=16)

    for i in range(len(lines)):
        plt.plot(T1, lines[i], linewidth=1, label=labels[i])
    plt.legend()
    plt.tight_layout()
    plt.savefig('plots/' + filename)
    plt.close('all')

plot(XNH3B1, 'XNH3B1.pdf', r'$x_\text{Ammonia,Bottom 1}$')
plot(XNH3T1, 'XNH3T1.pdf', r'$x_\text{Ammonia,Top 1}$')
plot(XNH3B2, 'XNH3B2.pdf', r'$x_\text{Ammonia,Bottom 2}$')
plot(XNH3T2, 'XNH3T2.pdf', r'$x_\text{Ammonia,Top 2}$')

# opt: T1 = 44.54 C, T2 = 10 C
plot(opt, 'opt.pdf', r'$x_\text{Amm}\dot{n}_\text{Amm}$ / \si{\kilo\mol\per\hour}')

plot_2d([
    np.reshape(data['NNH3B1'], (N, N)).transpose()[0],
    np.reshape(data['NNH3T1'], (N, N)).transpose()[0],
    np.reshape(data['NH2OB1'], (N, N)).transpose()[0],
    np.reshape(data['NH2OT1'], (N, N)).transpose()[0]
], 'flows.pdf', ['NNH3B1', 'NNH3T1', 'NH2OB1', 'NH2OT1'], r'$n_i$ / \si{\kilo\mol\per\hour}')

plot_2d([
    np.reshape(data['NNH3B1'] / (data['NNH3B1'] + data['NH2OB1']), (N, N)).transpose()[0],
    np.reshape(data['NNH3T1'] / (data['NNH3T1'] + data['NH2OT1']), (N, N)).transpose()[0]
], 'flows_x.pdf', ['xB', 'xT'], r'$x_\text{NH3}$ / -')

indices = [
    'NNH3B1',
    'NH2OB1',
    'NNH3T1',
    'NH2OT1',
    'NNH3B2',
    'NH2OB2',
    'NNH3T2',
    'NH2OT2',
]

for i in indices:
    plot(np.reshape(data[i], (N,N)), i + '.pdf', i)
