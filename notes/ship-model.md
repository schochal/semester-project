# Notes

## TEU vs. DWT

Goal: TEU of 1500
DWT: 190000

## GUDRUN MAERSK, as in https://doi.org/10.1021/acssuschemeng.2c04627
- Flag: Denmark
- Length: 367m
- beam: 43m
- DWT: 115700 t
- TEU: 8500
- Wärtsilä 12RT-flex96c, 68,640 kW at 102 rpm https://en.wikipedia.org/wiki/W%C3%A4rtsil%C3%A4-Sulzer_RTA96-C
- Brake Specific Fuel Consumption: 171 g / kWh --> this is the amount of fuel needed to produce one kWh

--> this is 1971.9 t of fuel for this journey. sensible? according to wikipedia article, it can be.
--> approx. 2000t is a good estimate

## Ship Used
- speed: 26.5 knots
- TEU: 8500
- DWT: 115700 t
- Travel time: 7 days
- Fuel: HFO (Heavy Fuel Oil)

## HFO
- Lower Heating Value: 39.57 MJ/kg, 39.57 MJ/l http://www.globalcombustion.com/oil-fuel-properties/
- ""                   39.00 MJ/kg, 38.20 MJ/l https://www.engineeringtoolbox.com/fuels-higher-calorific-values-d_169.html

## NH3
- LHV: 18.646 MJ/kg https://en.wikipedia.org/wiki/Heat_of_combustion
