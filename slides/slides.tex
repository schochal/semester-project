\documentclass[aspectratio=169]{beamer}

\usepackage{general}
\usepackage{slideStyle}
\usepackage{xurl}

\author{Alexander Schoch \\ \footnotesize Supervision: Margarita Athanasia Charalambous, Prof. Dr. Gonzalo Guillén Gosalbez}
\title{Process Simulation and techno-economic Assessment of NO$_x$-to-Ammonia in Ammonia powered Container Ships}
\institute{ETH Zürich}
\subtitle{Semester Project}
\date{August 9, 2023}

\begin{document}
    \begin{frame}
        \titlepage
    \end{frame}

    \section{Introduction}
    
    \begin{frame}{Motivation}
        \begin{itemize}
            \item Maritine shipping is one of the most efficient means of tranportation in terms of \si{\kilo\gram\of{CO\textsubscript{2}-eq}\per\ton\per\kilo\meter}
            \item It is still responsible for a lot of emissions\footfullcite{Kramel2021}
            \begin{itemize}
                \item CO\textsubscript{2}: \SI{2.9}{\percent}
                \item NO$_x$ (NO \& NO\textsubscript{2}): \SI{15}{\percent}
            \end{itemize}
            \item There is a need to decarbonization
        \end{itemize}
    \end{frame}

    \begin{frame}{A little bit of History}
        \begin{itemize}
            \item Ships run on Diesel / Heavy Fuel Oil (HFO)
            \item Engines release NO$_x$ (NO \& \ce{NO2})
            \item International Maritime Organization introduces NO$_x$ regulations
            \item Shipping companies react with two NO$_x$ reduction measures:
            \begin{itemize}
                \item Selective Catalytic Reduction
                \begin{equation*}
                    \schemestart
                    \arrow{0}[,.26]
                    \ce{4NH3} \+ \ce{4NO} \+ \ce{O2}
                    \arrow{->}
                    \ce{4N2} \+ \ce{6H2O}
                    \schemestop
                \end{equation*} \\[-.5cm]
                \begin{equation*}
                    \schemestart
                    \ce{4NH3} \+ \ce{2NO} \+ \ce{2NO2}
                    \arrow{->}
                    \ce{4N2} \+ \ce{6H2O}
                    \schemestop
                \end{equation*}\\[.5cm]
                \item Exhaust Gas Recirculation (EGR)
            \end{itemize}
        \end{itemize}
    \end{frame}
    
    \begin{frame}{Alternative Fuels}
        \begin{itemize}
            \item Carbon-Based
            \begin{itemize}
                \item LNG / LPG
                \item Methanol
            \end{itemize}
            \item Carbon-Free
            \begin{itemize}
                \item Hydrogen Fuel Cells
                \item Lithium-Ion Batteries
                \item Ammonia
            \end{itemize}
        \end{itemize}
        \begin{block}{Of the carbon-free options, ammonia is the most promising:}
            \begin{itemize}
                \item Relatively high energy density
                \item Relatively cheap
                \item Easy storage (similar to LPG)
            \end{itemize}
        \end{block}
    \end{frame}
    
    \begin{frame}{The Problem with Ammonia}
        \begin{block}{Ammonia combustion releases NO$_x$ (NO \& NO\textsubscript{2})}
            \begin{itemize}
                \item Indirect greenhouse gases
                \item Converted to nitric acid in atmosphere
                \item Converted no N\textsubscript{2}O in soils
                \item N\textsubscript{2}O has a \SI{100}{\year} GWP of about \num{300}!
            \end{itemize}
        \end{block}
    \end{frame}
    
    \begin{frame}{Potential NO$_x$ Reduction Processes}
        \begin{block}{Selective Catalytic Reduction (SCR)}
            \begin{itemize}
                \item Already estabilshed for Diesel / HFO ships
                \item Works for low NO$_x$ concentrations
                \item \enquote{Wastes} NO$_x$ as \ce{N2}
            \end{itemize}
        \end{block}
        \begin{block}{NO$_x$-to-Ammonia (NTA)}
            \begin{itemize}
                \item Proposed by Matsumoto et al.\footfullcite{Matsumoto2022}
                \item Converts NO$_x$ back to NH\textsubscript{3} (fuel regeneration)
                \item Needs a H\textsubscript{2} / CO source
            \end{itemize}
            \begin{equation*}
                \schemestart
                \arrow{0}[,1.05]\ce{2NO} \+ \ce{5H2} \arrow{->} \ce{2NH3} \+ \ce{2H2O}
                \schemestop
            \end{equation*}
            \begin{equation*}
                \schemestart
                \ce{2NO} \+ \ce{5CO} \+ \ce{3H2O} \arrow{->} \ce{2NH3} \+ \ce{5CO2}
                \schemestop
            \end{equation*}
        \end{block}
    \end{frame}


    \begin{frame}{Aim of the Project}
        \begin{itemize}
            \item Retrofit an existing ammonia powered vessel
            \item Model an NO$_x$-to-Ammonia process, as described by Matsumoto et al.
            \item Compare to different scenarios
            \item Economic and Environmental Assessement
        \end{itemize}

        \begin{block}{Scenarios}
            \begin{itemize}
                \item Diesel / HFO (BAU)
                \item Ammonia + SCR (base case, BC)
                \item Ammonia + NTA (NTA)
            \end{itemize}

        \end{block}
    \end{frame}

    \begin{frame}{Ammonia Production Processes}
        \begin{figure}[ht]
            \begin{subfigure}{.33\linewidth}
            \centering
            \begin{tikzpicture}[scale=0.75, every node/.style={scale=0.75}]
                \node[circle, draw] (CH4) at (0,0) {\ce{CH4}};
                \node[circle, draw] (H2O) at (2,0) {\ce{H2O}};

                \node[draw] (steam_reforming) at (1,-1.5) {Steam Reforming};
                \node[draw] (water_gas_shift) at (1, -2.5) {Water-Gas Shift};

                \node[circle, draw] (CO2) at (0, -4) {\ce{CO2}};
                \node[circle, draw] (H2) at (2, -4) {\ce{H2}};
                \node[circle, draw] (N2) at (4, -4) {\ce{N2}};

                \node[draw] (haber_bosch) at (3, -5.5) {Haber-Bosch Process};

                \node[circle, draw] (NH3) at (3, -7) {\ce{NH3}};

                \draw[-latex, thick] (CH4) -- (steam_reforming);
                \draw[-latex, thick] (H2O) -- (steam_reforming);
                \draw[-latex, thick] (steam_reforming) -- (water_gas_shift);
                \draw[-latex, thick] (water_gas_shift) -- (CO2);
                \draw[-latex, thick] (water_gas_shift) -- (H2);
                \draw[-latex, thick] (H2) -- (haber_bosch);
                \draw[-latex, thick] (N2) -- (haber_bosch);
                \draw[-latex, thick] (haber_bosch) -- (NH3);
            \end{tikzpicture}
            \caption{Gray/Brown Ammonia}
            \label{fig:gray_ammonia}
            \end{subfigure}\hfill
            \begin{subfigure}{.33\linewidth}
            \centering
            \begin{tikzpicture}[scale=0.75, every node/.style={scale=0.75}]
                \node[circle, draw] (CH4) at (0,0) {\ce{CH4}};
                \node[circle, draw] (H2O) at (2,0) {\ce{H2O}};

                \node[draw] (steam_reforming) at (1,-1.5) {Steam Reforming};
                \node[draw] (water_gas_shift) at (1, -2.5) {Water-Gas Shift};

                \node[circle, draw] (CO2) at (0, -4) {\ce{CO2}};
                \node[circle, draw] (H2) at (2, -4) {\ce{H2}};
                \node[circle, draw] (N2) at (4, -4) {\ce{N2}};

                \node[draw] (haber_bosch) at (3, -5.5) {Haber-Bosch Process};
                \node[draw] (CCUS) at (0, -5.5) {CCUS};

                \node[circle, draw] (NH3) at (3, -7) {\ce{NH3}};

                \draw[-latex, thick] (CH4) -- (steam_reforming);
                \draw[-latex, thick] (H2O) -- (steam_reforming);
                \draw[-latex, thick] (steam_reforming) -- (water_gas_shift);
                \draw[-latex, thick] (water_gas_shift) -- (CO2);
                \draw[-latex, thick] (water_gas_shift) -- (H2);
                \draw[-latex, thick] (H2) -- (haber_bosch);
                \draw[-latex, thick] (N2) -- (haber_bosch);
                \draw[-latex, thick] (haber_bosch) -- (NH3);
                \draw[-latex, thick] (CO2) -- (CCUS);
            \end{tikzpicture}
            \caption{Blue Ammonia}
            \label{fig:blue_ammonia}
            \end{subfigure}\hfill
            \begin{subfigure}{.33\linewidth}
            \centering
            \begin{tikzpicture}[scale=0.75, every node/.style={scale=0.75}]
                \node[circle, draw, align=center] (renewable_power) at (1, -1) {ren.\\ power};
                \node[circle, draw] (H2O) at (3,-1) {\ce{H2O}};
                \node[circle, draw] (H2) at (2, -4) {\ce{H2}};

                \node[circle, draw] (N2) at (4, -4) {\ce{N2}};

                \node[draw] (electrolysis) at (2, -2.5) {Electrolysis};
                \node[draw] (haber_bosch) at (3, -5.5) {Haber-Bosch Process};

                \node[circle, draw] (NH3) at (3, -7) {\ce{NH3}};

                \draw[-latex, thick] (renewable_power) -- (electrolysis);
                \draw[-latex, thick] (H2O) -- (electrolysis);
                \draw[-latex, thick] (electrolysis) -- (H2);
                \draw[-latex, thick] (H2) -- (haber_bosch);
                \draw[-latex, thick] (N2) -- (haber_bosch);
                \draw[-latex, thick] (haber_bosch) -- (NH3);
            \end{tikzpicture}
            \caption{Green Ammonia}
            \label{fig:green_ammonia}
            \end{subfigure}
            \caption{Ammonia Production Pathways.}
        \end{figure}
    \end{frame}
    
    \begin{frame}{Rough Setup}
        \begin{figure}
            \begin{minipage}{.66\linewidth}
                \centering
                \begin{tikzpicture}[scale=0.8, every node/.style={scale=0.8}]
                    \pic (tank) at (0,0) {tank};
                    \pic (cracker) at (4, 0) {packed bed reactor};
                    \pic (engine) at (0, -4) {tube bundle reactor};
                    \pic (nta) at (4, -4) {packed bed reactor};

                    \node at (0,0) {\ce{NH3} tank};
                    \node [above] at (cracker-top) {\ce{NH3} cracker};
                    \node [below] at (engine-bottom) {engine};
                    \node [below] at (nta-bottom) {NTA};

                    \draw [main stream] (tank-right) -- (cracker-left);
                    \draw [main stream] (tank-bottom) -- (engine-top);
                    \draw [main stream, dashed] (cracker-utility bottom left) -- (engine-top right);
                    \draw [main stream, dashed] (cracker-bottom) -- (nta-top);
                    \draw [main stream, dotted] (engine-right) -- (nta-left);
                    \draw [main stream] (nta-utility top left) -- (tank-bottom right);

                    \draw (5.5, -3) rectangle (9.2, -1);

                    \draw [main stream] (6, -1.5) -- ++(2,0) node [right] {\ce{NH3}};
                    \draw [main stream, dashed] (6, -2) -- ++(2,0) node [right] {\ce{H2}};
                    \draw [main stream, dotted] (6, -2.5) -- ++(2,0) node [right] {\ce{NO}};
                    \end{tikzpicture}
            \end{minipage}
            \begin{minipage}{.33\linewidth}
                \begin{itemize}
                    \item \ce{NH3} is fed to engine and cracker
                    \item Both then feed to NTA
                    \item NTA recycles back to Tank/Engine
                \end{itemize}
            \end{minipage}
        \end{figure}
    \end{frame}
    
    \begin{frame}{Case Study}
        \begin{block}{Ship Similar to GUDRUN Maersk}
            \begin{itemize}
                \item DWT: \SI{115700}{\ton}
                \item Capacity: \SI{8500}{TEU}
                \item Engine: \textsf{Wärtsilä 12RT-flex96c} (\SI{68.64}{\mega\watt})
            \end{itemize}
        \end{block}
        
        \begin{block}{Trip}
            \begin{itemize}
                \item $t = \SI{1}{week}$
                \item $v = \SI{26.5}{knots}$
            \end{itemize}
        \end{block}
    \end{frame}


    % remove figure captions
    \section{Procedure}
    \begin{frame}{Process}
        \begin{figure}
            \begin{minipage}{.66\linewidth}
                \begin{tikzpicture}[scale=0.5, every node/.style={scale=0.5}]
                    \draw [main stream] (0,0) -- node [midway, above] {\ce{NH3} feed} (2,0);
                    \draw [pattern=north west lines, pattern color=palesilver, thick] (2,0) -- (3.7321,1) -- (3.7321,-1) -- cycle;
                    \draw [main stream] (3.7321,-.5) -- (4.5, -.5) -- (4.5, -1.75) -- (0, -1.75) -- (0, -4) -- (2, -4);

                    % Cracker
                    \pic (HE NH3) at (6, .5) {heat exchanger};
                    \pic (cracker) at (9, .5) {packed bed reactor};
                    \pic (flash cracker) at (12, .5) {tank reactor};
                    \draw[pattern=north west lines, pattern color=palesilver, thick] (12, -1.5) -- (13, -3.2321) -- (11, -3.2321) -- cycle;

                    \node [above] at (cracker-top) {cracker};


                    \draw [main stream] (3.7321, .5) -- (HE NH3-pipes left);
                    \draw [main stream] (HE NH3-pipes right) -- (cracker-left);
                    \draw [main stream] (cracker-right) -- (flash cracker-left);
                    \draw [main stream] (flash cracker-bottom) -- (12, -1.5);
                    \draw [main stream] (flash cracker-top) -- node [midway, left] {\ce{N2} purge} (12, 3);

                    \draw [main stream] (11.5,-3.2321) -- (11.5,-3.5) -- (10,-3.5) -- (10,-2.25) -- (0.5,-2.25) -- (0.5, -3.5) -- (2,-3.5);
                    \draw [main stream] (12.5,-3.2321) -- (12.5,-3.5) -- (13.5, -3.5);


                    % Engine
                    \pic (HE engine) at (5.5, -4) {heat exchanger};
                    \pic (engine) at (8, -4) {tube bundle reactor};
                    \draw[pattern=north west lines, pattern color=palesilver, thick] (2,-3) -- (2,-5) -- (3.7321,-4) -- cycle;

                    \node [below] at (engine-bottom) {engine};

                    \draw [main stream] (0,-4.5) -- node [midway, below] {air feed} (2,-4.5);
                    \draw [main stream] (3.7321,-4) -- (HE engine-pipes left);
                    \draw [main stream] (HE engine-pipes right) -- (engine-left);
                    \draw [main stream] (engine-right) -- (9.5,-4) -- (9.5, -4.5) -- (13.5, -4.5);


                    % NTA
                    \draw [pattern=north west lines, pattern color=palesilver, thick] (13.5,-3) -- (13.5,-5) -- (15.2321,-4) -- cycle;
                    \pic[rotate=90] (HE NTA) at (16, -5) {heat exchanger};
                    \pic[rotate=90] (NTA) at (16, -7) {packed bed reactor};

                    \node [left] at (NTA-top) {NTA};

                    \draw [main stream] (15.2321,-4) -- (16, -4) -- (HE NTA-pipes right);
                    \draw [main stream] (HE NTA-pipes left) -- (NTA-right);

                    % Recycle
                    \pic (HF1) at (13,-9) {heat exchanger};
                    \pic (FL1) at (10, -9) {tank reactor};
                    \pic (HF2) at (7,-9) {heat exchanger};
                    \pic (FL2) at (4, -9) {tank reactor};

                    \node at (10,-9) {Flash 1};
                    \node at (4,-9) {Flash 2};

                    \draw [main stream] (NTA-left) -- (16,-9) -- (HF1-pipes right);
                    \draw [main stream] (HF1-pipes left) -- (FL1-right);
                    \draw [main stream] (FL1-top) -- (10,-7) -- (8.5,-7) -- (8.5,-9) -- (HF2-pipes right);
                    \draw [main stream] (FL1-bottom) -- ++(0,-2) node[midway, right]{\ce{H2O} purge};
                    \draw [main stream] (HF2-pipes left) -- (FL2-right);
                    \draw [main stream] (FL2-top) -- (4,-7) -- (0,-7) node[midway, below] {\ce{NH3} recycle};
                    \draw [main stream] (FL2-bottom) -- ++(0,-2) node[midway, right]{\ce{H2O} purge};

                    \draw [dashed] (5,-.5) rectangle (10.5,1.5);
                    \node [draw, right, fill=white] at (4.75, 1.5) {section cracker};
                    \draw [dashed] (0.25, -6) rectangle (9.25, -2.5);
                    \node [draw, right, fill=white] at (0, -6) {section engine};
                    \draw [dashed] (13.25,-2.75) rectangle (16.75,-8.25);
                    \node [draw, right, fill=white] at (14, -2.75) {section NTA};
                    \draw [dashed] (0.25,-6.5) rectangle (13.75,-12);
                    \node [draw, right, fill=white] at (0, -12) {section recycle};
                    %\draw [main stream] (flash NTA-top) -- ++(-.5, 0) -- ++(0,2) node [above] {recycle};
                    %\draw [main stream] (flash NTA-bottom) -- ++(.5, 0) -- ++(0, 2) node [above] {purge};
                    \end{tikzpicture}
                \end{minipage}
                \begin{minipage}{.33\linewidth}
                    Process Flowsheet, modelled in Aspen Plus V11
                \end{minipage}
        \end{figure}
    \end{frame}

    \begin{frame}{Engine}
        \begin{figure}
            \begin{minipage}{.66\linewidth}
                \includegraphics[width=\linewidth]{../combustion_python/plots/sans-serif.pdf}
            \end{minipage}\hfill
            \begin{minipage}{.33\linewidth}
                Numeric Integration of Rate Equations\footnotemark[3]\footnotemark[4] using python.
            \end{minipage}
        \end{figure}
        \footnotetext[3]{\fullcite{Duynslaegher2012}}
        \footnotetext[4]{\fullcite{Stagni2020}}
        \addtocounter{footnote}{2}
    \end{frame}
    
    \begin{frame}
        Thus, modelling was done in Aspen Plus V11.
        \begin{itemize}
            \item RGibbs produced Hydrogen
            \item Difficult to find modelling data
        \end{itemize}
        \begin{block}{Final Configuration}
            \begin{itemize}
                \item RStoic with combustion reactions (\SI{100}{\percent} NH\textsubscript{3} to NO conversion)
                \item Air-to-Fuel ratio: 8
                \item No H\textsubscript{2} stream
                \item $T = \SI{651}{\celsius}$
            \end{itemize}
        \end{block}
        \begin{figure}
            \centering
            \begin{tikzpicture}[scale=0.7, every node/.style={scale=0.7}]
                \pic (HE engine) at (5.5, -4) {heat exchanger};
                \pic (engine) at (8, -4) {tube bundle reactor};
                \draw[pattern=north west lines, pattern color=palesilver, thick] (2,-3) -- (2,-5) -- (3.7321,-4) -- cycle;

                \node [below] at (engine-bottom) {engine};

                \draw [main stream] (0,-3.3) -- node [midway, above] {(H\textsubscript{2} feed)} (2,-3.3);
                \draw [main stream] (0,-4) -- node [midway, above] {NH\textsubscript{3} feed} (2,-4);
                \draw [main stream] (0,-4.7) -- node [midway, above] {air feed} (2,-4.7);
                \draw [main stream] (3.7321,-4) -- (HE engine-pipes left);
                \draw [main stream] (HE engine-pipes right) -- (engine-left);
                \draw [main stream] (engine-right) -- ++(2, 0) node[midway, above] {to NTA};
            \end{tikzpicture}
        \end{figure}
    \end{frame}
    
    \begin{frame}{Cracker}
        The cracker is used to produce hydrogen. Here, \ce{NH3} is used as hydrogen source.
        \begin{block}{Final Configuration}
            \begin{itemize}
                \item RPlug with reaction (membrane reactor):
                    \begin{equation*}
                        \schemestart
                        \ce{2NH3} \arrow{->} \ce{N2} \+ \ce{3H2}
                        \schemestop
                    \end{equation*}
                \item $T = \SI{700}{\kelvin}$
                \item Kinetic Data from Kim et al.\footfullcite{Kim2018}
            \end{itemize}
        \end{block}
        \begin{figure}
            \centering
            \begin{tikzpicture}[scale=0.7, every node/.style={scale=0.7}]
                \pic (HE NH3) at (6, .5) {heat exchanger};
                \pic (cracker) at (9, .5) {packed bed reactor};
                \pic (flash cracker) at (12, .5) {tank reactor};

                \node [above] at (cracker-top) {cracker};

                \draw [main stream] (3.7321, .5) -- (HE NH3-pipes left) node[midway, above] {NH\textsubscript{3} feed};
                \draw [main stream] (HE NH3-pipes right) -- (cracker-left);
                \draw [main stream] (cracker-right) -- (flash cracker-left);
                \draw [main stream] (flash cracker-bottom) -- (12, -1.5) node[midway, right] {to NTA};
                \draw [main stream] (flash cracker-top) -- node [midway, right] {\ce{N2} purge} (12, 2.5);
             \end{tikzpicture}
        \end{figure}
    \end{frame}
    
    \begin{frame}{NTA Reactor}
        The NTA produces \ce{NH3} from \ce{NO}
        \begin{itemize}
            \item RGibbs results in conversions of $X_\text{NO} = \SI{3}{\percent}$ (Matsumoto et al.: $X_\text{NO} = \SI{90}{\percent}$\footfullcite{Matsumoto2022})
        \end{itemize}
        \begin{block}{Final Configuration}
            \begin{itemize}
                \item RStoic with reaction
                \begin{equation*}
                    \schemestart
                    \ce{2NO} \+ \ce{5H2} \arrow{->} \ce{2NH3} \+ \ce{2H2O}
                    \schemestop
                \end{equation*}
                \item $T = \SI{250}{\celsius}$
                \item $X_\text{NO} = 0.9$
                \item CO reaction not implemented
            \end{itemize}
        \end{block}
        \begin{figure}
            \centering
            \begin{tikzpicture}[scale=0.7, every node/.style={scale=0.7}]
                \draw [pattern=north west lines, pattern color=palesilver, thick] (13.5,-3) -- (13.5,-5) -- (15.2321,-4) -- cycle;
                \pic (HE NTA) at (17, -4) {heat exchanger};
                \pic (NTA) at (19, -4) {packed bed reactor};

                \node [above] at (NTA-top) {NTA};

                \draw [main stream] (10, -3.5) -- (13.5, -3.5) node [midway, above] {\ce{H2} from cracker};
                \draw [main stream] (10, -4.5) -- (13.5, -4.5) node [midway, above] {engine exhaust};
                \draw [main stream] (15.2321,-4) -- (16, -4) -- (HE NTA-pipes left);
                \draw [main stream] (HE NTA-pipes right) -- (NTA-left);
                \draw [main stream] (NTA-right) -- ++(2, 0) node[midway, above] {to recycle};
             \end{tikzpicture}
        \end{figure}
    \end{frame}
    
    \begin{frame}{Recycle}
        The recycle process removes water from the mixture
        \begin{block}{Final Configuration}
            \begin{itemize}
                \item Two flashes
                \item $T_1 = \SI{44.5}{\celsius}$
                \item $T_2 = \SI{10}{\celsius}$
                \item \SI{85}{\percent} \ce{NH3} recovery (one flash: \SI{93}{\percent})
                \item \SI{97}{\percent} water removal (one flash: \SI{72}{\percent})
            \end{itemize}
        \end{block}
        \begin{figure}
            \centering
            \begin{tikzpicture}[scale=0.7, every node/.style={scale=0.7}]
                \pic (HF1) at (-13,-9) {heat exchanger};
                \pic (FL1) at (-10, -9) {tank reactor};
                \pic (HF2) at (-7,-9) {heat exchanger};
                \pic (FL2) at (-4, -9) {tank reactor};

                \node at (-10,-9) {Flash 1};
                \node at (-4,-9) {Flash 2};

                \draw [main stream] (-16,-9) -- (HF1-pipes left) node [midway, above] {from NTA};
                \draw [main stream] (HF1-pipes right) -- (FL1-left);
                \draw [main stream] (FL1-top) -- (-10,-7) -- (-8.5,-7) -- (-8.5,-9) -- (HF2-pipes left);
                \draw [main stream] (FL1-bottom) -- ++(0,-1) node[midway, right]{\ce{H2O} purge};
                \draw [main stream] (HF2-pipes right) -- (FL2-left);
                \draw [main stream] (FL2-top) -- (-4,-7) -- (0,-7) node[midway, below] {\ce{NH3} recycle};
                \draw [main stream] (FL2-bottom) -- ++(0,-1) node[midway, right]{\ce{H2O} purge};
             \end{tikzpicture}
        \end{figure}
    \end{frame}

    \begin{frame}{\vphantom{x}}
        \begin{figure}
            \begin{minipage}{.66\linewidth}
                \includegraphics[width=\linewidth]{../separation_water_ammonia/plots/opt.pdf}
            \end{minipage}\hfill
            \begin{minipage}{.33\linewidth}
                Sensitivity analysis for serial flashes.
            \end{minipage}
        \end{figure}
    \end{frame}

    \section{Results}
    \begin{frame}{Results}
        \begin{block}{Streams}
            \begin{itemize}
                \item Engine: \SI{5.64}{\gram\of{NH\textsubscript{3}}\per\ton\per\kilo\meter} (\SI{32}{\ton\per\hour})
                \item Cracker: \SI{11.5}{\gram\of{NH\textsubscript{3}}\per\ton\per\kilo\meter} (\SI{66}{\ton\per\hour})
                \item Recycle: \SI{4.30}{\gram\of{NH\textsubscript{3}}\per\ton\per\kilo\meter} (\SI{24}{\ton\per\hour}, \SI{25}{\percent} recovery overall)
            \end{itemize}
        \end{block}
        \begin{block}{Fuel Usage}
            \begin{itemize}
                \item BAU: \SI{2.07}{\gram\of{Diesel}\per\ton\per\kilo\meter}
                \item Engine Only: \SI{5.64}{\gram\of{NH\textsubscript{3}}\per\ton\per\kilo\meter}
                \item Engine + NTA: \SI{12.88}{\gram\of{NH\textsubscript{3}}\per\ton\per\kilo\meter}
                \item Engine + SCR: \SI{11.27}{\gram\of{NH\textsubscript{3}}\per\ton\per\kilo\meter}
            \end{itemize}
        \end{block}
    \end{frame}
    
    \begin{frame}{Energy Integration}
        \begin{figure}[ht]
            \centering
            \begin{tikzpicture}[scale=0.85, every node/.style={scale=0.85}]
                \draw[latex-, blue, thick] (0,0.5) node[above]{\SI{25}{\celsius}} -- (2.5, 0.5) -- (3,0) -- (6, 0) -- (6.5,.5) -- (8.5,.5) node[midway, above, opacity=.8] {sea water} -- (9,0) -- (12,0) -- (12.5, 0.5) -- (15,0.5) node[above] {\SI{5}{\celsius}};
                \draw[blue, thick] (2.5,0.5) -- (3,1) -- (6,1) -- (6.5,.5) -- (8.5,.5) -- (9,1) -- (12,1) -- (12.5, 0.5);
                \draw[-latex, red, thick] (0,-1) node[above]{\SI{608}{\celsius}} -- (2.5,-1) -- (3,-.5) -- (12,-.5) -- (12.5,-1) -- (15,-1);
                \draw[-latex, red, thick] (0,-1) node[above]{\SI{608}{\celsius}} -- (2.5,-1) -- (3,-1.5) -- (12,-1.5) -- (12.5,-1) -- (13,-1) -- (15,-1) node[above] {\SI{250}{\celsius}};
                \draw[-latex, red, thick] (0,-2.5) node[above]{\SI{250}{\celsius}} -- ++(15,0) node[above] {\SI{44.5}{\celsius}};
                \draw[-latex, red, thick] (0,-3.5) node[above]{\SI{44.5}{\celsius}} -- ++(15,0) node[midway, above, opacity=.8] {heat exchanger Flash 2} node[above] {\SI{10}{\celsius}};
                \draw[latex-, blue, thick] (0,-4.5) node[above]{\SI{426.9}{\celsius}} -- ++(15,0) node[midway, above, opacity=.8] {heat exchanger cracker} node[above] {\SI{25}{\celsius}};

                \draw[thick] (13,-4.5) node[circle, fill, inner sep=2]{} -- (13, -1) node[circle, fill, inner sep=2]{};
                \draw[thick] (11.5,-2.5) node[circle, fill, inner sep=2]{} -- (11.5, 0) node[circle, fill, inner sep=2]{};
                \draw[thick] (10.5,-3.5) node[circle, fill, inner sep=2]{} -- (10.5, 1) node[circle, fill, inner sep=2]{};
                \draw[thick] (7.5,-2.5) node[circle, fill, inner sep=2]{} -- (7.5, .5) node[circle, fill, inner sep=2]{};
                \draw[thick] (4.5,-2.5) node[circle, fill, inner sep=2]{} -- (4.5, 0) node[circle, fill, inner sep=2]{};
                \draw[thick] (4,-4.5) node[circle, fill, inner sep=2]{} -- (4, -1.5) node[circle, fill, inner sep=2]{};
                \draw[thick] (3.5,-.5) node[circle, fill, inner sep=2]{} -- (3.5, 1) node[circle, fill, inner sep=2]{};

                \node[red, fill=white, opacity=.8] at (7.5,-1) {heat exchanger NTA};
                \node[red, above, fill=white, opacity=.8] at (7.5,-2.5) {heat exchanger Flash 1};

                \node [below, blue] at (5.5,1) {\footnotesize \SI{13.9}{\celsius}};
                \node [below, blue] at (9.5 ,1) {\footnotesize \SI{9.9}{\celsius}};
                \node [above, red] at (6 ,-.5) {\footnotesize \SI{278}{\celsius}};
                \node [below, red] at (6 ,-2.5) {\footnotesize \SI{66}{\celsius}};
                \node [below, red] at (9 ,-2.5) {\footnotesize \SI{54.9}{\celsius}};
                \node [above, blue] at (12 ,-4.5) {\footnotesize \SI{159}{\celsius}};

                \node [right] at (3.5,.5) {\footnotesize \SI{5265}{\square\meter}};
                \node [right] at (4,-3) {\footnotesize \SI{19754}{\square\meter}};
                \node [left] at (4.5,-1) {\footnotesize \SI{14282}{\square\meter}};
                \node [right] at (7.5,-.25) {\footnotesize \SI{1340}{\square\meter}};
                \node [right] at (10.5,-3) {\footnotesize \SI{5589}{\square\meter}};
                \node [right] at (11.5,-2) {\footnotesize \SI{533}{\square\meter}};
                \node [right] at (13,-2) {\footnotesize \SI{5443}{\square\meter}};
            \end{tikzpicture}
            Heat Exchanger Network, calculated with Aspen Energy Analyzer V11.
        \end{figure}
    \end{frame}
    
    \begin{frame}{CAPEX}
        \begin{itemize}
            \item Cost estimation using Sinnot/Towler\footfullcite{Sinnot2020}
            \begin{equation*}
                C_\text{e} = a + bS^n
            \end{equation*}
            \item If size parameter out of range: Split into multiple parts within rage
            \item Corrected for inflation (using CEPCI), FPP, FIC, FE, FPC \& FEE % piping, instrumentation, electric, painting & coating, edification
            \item No location parameter
            \item Lifetime: \SI{15}{\year}
            \item Base Case is assumed to have the same CAPEX
        \end{itemize}
    \end{frame}

    \begin{frame}
        \begin{figure}
            \begin{minipage}{.5\linewidth}
                \includegraphics[width=\linewidth]{../CAPEX_OPEX/plots/CAPEX.pdf}
            \end{minipage}\hfill
            \begin{minipage}{.5\linewidth}
                \caption{Distribution of total CAPEX of \SI{127e6}{\USD} (annual: \SI{16.7e6}{\USD\per\year})}
            \end{minipage}
        \end{figure}
    \end{frame}
    
    \begin{frame}{OPEX}
        \begin{minipage}{.5\linewidth}
            \begin{figure}[H]
                \includegraphics[width=\linewidth]{../CAPEX_OPEX/plots/ammonia_prices.pdf}
            \end{figure}
        \end{minipage}\hfill
        \begin{minipage}{.5\linewidth}
            \begin{itemize}
                \item Average ammonia price (June 2023) for each type\footnotemark
                \item Average diesel price
                \item Only fuel costs
            \end{itemize}
        \end{minipage}
        \footnotetext{\fullcite{ammonia_price}}
    \end{frame}

    % instead of caption, add insight that NTA > BC, even though we regenerate
    \begin{frame}{Total Cost}
        \begin{figure}
            \begin{minipage}{.66\linewidth}
                \includegraphics[width=\linewidth]{../CAPEX_OPEX/plots/CAPEX_OPEX.pdf}
            \end{minipage}
            \begin{minipage}{.33\linewidth}
                \begin{itemize}
                    \item NTA is more expensive than BC, despite regeneration
                    \item Green ammonia is by far the most expensive
                    \item CAPEX is comparitively small
                \end{itemize}
            \end{minipage}
        \end{figure}
    \end{frame}
    
    \begin{frame}{LCA}
        \begin{minipage}[t]{.5\linewidth}
            \begin{block}{Aim}
                \begin{itemize}
                    \item Compare emissions to base case and BAU
                \end{itemize}
            \end{block}
            \begin{block}{Scope}
                \begin{itemize}
                    \item Gradle-to-Propulsion
                \end{itemize}
            \end{block}
            \begin{block}{Functional Unit}
                \begin{itemize}
                    \item \SI{1}{\ton\kilo\meter}
                \end{itemize}
            \end{block}
        \end{minipage}\hfill
        \begin{minipage}[t]{.5\linewidth}
            \begin{block}{Inventory Analysis}
                \begin{itemize}
                    \item Developed Simulation
                \end{itemize}
            \end{block}
            \begin{block}{Impact Analysis}
                \begin{itemize}
                    \item Global Warming Potential
                    \begin{itemize}
                        \item GWP Diesel: Negri et al.\footnotemark[9]%\footfullcite{Negri2022}
                        \item GWP Ammonia: D'Angelo et al.\footnotemark[10]%\footfullcite{DAngelo2021}
                    \end{itemize}
                    \item NO assumed to have a GWP of 8.5\footnotemark[11]
                    \item Direct \ce{N2O} emissions ignored
                \end{itemize}
            \end{block}
        \end{minipage}
        \footnotetext[9]{\fullcite{Negri2022}}
        \footnotetext[10]{\fullcite{DAngelo2021}}
        \footnotetext[11]{\fullcite{Lammel1995}}
    \end{frame}

    \begin{frame}{\vphantom{x}}
        \begin{figure}
            \begin{minipage}{.66\linewidth}
                \includegraphics[width=\linewidth]{../CAPEX_OPEX/plots/LCA.pdf}
            \end{minipage}
            \begin{minipage}{.33\linewidth}
                \begin{itemize}
                    \item All \ce{NH3} cases are worse than BAU
                    \item BC has a \SI{95}{\percent}, NTA a \SI{90}{\percent} NO conversion
                    \item Even green ammonia does not have a big enough emission reduction compared to BAU
                \end{itemize}
            \end{minipage}
        \end{figure}
    \end{frame}
    
    \begin{frame}{What can we do about this?}
        \begin{itemize}
            \item Research into other \ce{H2} sources, such as fuel cells
            \item More detailed engine modelling
            \item Higher NTA efficiencies, maybe at higher temperatures
            \item Better \ce{NH3} recovery, e.g. via ammonia stripping
        \end{itemize}
    \end{frame}

    \begin{frame}{Conclusions}
        \begin{itemize}
            \item The current setup performs overall worse than BAU
            \item More research into ammonia combustion and NTA needed
            \item Might be feasible in the future, but not right now
        \end{itemize}
    \end{frame}
    
    \begin{frame}{Slides / Report}
        \begin{itemize}
            \item Slides \& Report available at \href{https://gitlab.ethz.ch/schochal/semester-project}{https://gitlab.ethz.ch/schochal/semester-project}
        \end{itemize}
    \end{frame}

    \begin{frame}
        \begin{center}
            \Huge Thank You! \par\bigskip
            \Large Time for Questions
        \end{center}
    \end{frame}

    \begin{frame}{LCA: Details}
        \begin{block}{\ce{NH3} Production Emissions}
            \begin{itemize}
                \item Much higher fuel usage
                \item approx. \SI{1.6}{\ton\of{CO\textsubscript{2}}\per\ton\of{NH\textsubscript{3}}} for gray ammonia
            \end{itemize}
        \end{block}
        \begin{block}{\ce{NH3} Combustion Emissions}
            \begin{itemize}
                \item Engine: \SI{100}{\percent} NO yield
                \item \SI{100}{\year} GWP of NO: 8.5
                \item NTA: \SI{90}{\percent} conversion
            \end{itemize}
        \end{block}
    \end{frame}
    
\end{document}
