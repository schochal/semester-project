# Slides Notes

## Introduction

### 2

- Maritine Shipping is responsible for a large part of emissions worldwide
- There has been a need to decarbonize it for 30 years now 

### 3

- NOx regulations: 3 Tiers, last one started in 2016
- SCR: NO --> N2
- EGR: recycle into engine, tested for cars

### 4

- LNG is the most used alternative
- Methanol also looks really promising
- There are small ships powered with batteres
- ammonia has a high potential of the carbon-free options
- reason for not using H2 / batteries: low energy density --> lots of weight and thus reduced cargo

### 5

- indirect greenhouse gas: converted to HNO3 --> N2O
- GWP of about 9 over 100 years

### 6

- SCR is not tested for larger NOx concentrations yet
- NH3 can recycle instead of waste the NOx

### 10

- DWT: deadweight tonnage, the amount of freight a ship can carry, incl fuel and personnel
- TEU: twenty-foot equivalent unit

## 12

- rate equations
- take rate expression and apply runge-kutta
- show in graphic that this does not add up

## 18

- show that the amouns change rapidly 

## 19

- explain

## 22

- incl. annual capital change ratio

## 24

- capex is low

## 26

- blue / green ammonia only viable solutions
- NO has a large impact
- difference in NO emissions from different efficiencies

## Procedure

## Results

## Discussion

## Conclusion
